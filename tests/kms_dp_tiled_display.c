/*
 * Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Authors:
 *  Madhumitha Tolakanahalli Pradeep
 *      <madhumitha.tolakanahalli.pradeep@intel.com>
 *
 * Display Port Tiled Display Test
 * This test parses the tile information of the connectors that have TILE
 * property set, sets up the framebuffer with correct offsets corresponding to
 * the tile offsets and does an atomic modeset with two CRTCs for two
 * connectors.
 *
 * This test currently supports only horizontally tiled displays, in line with
 * the displays supported by the kernel at the moment.
 */

#include "poll.h"
#include "drm_mode.h"
#include "drm_fourcc.h"
#include "igt.h"

typedef struct {
	int drm_fd;
	int num_h_tiles;
	igt_display_t *display;
	struct igt_fb *fb_test_pattern;
	enum igt_commit_style commit;
} data_t;

typedef struct {
	igt_output_t *output;
	igt_tile_info_t *tile;
	drmModeConnectorPtr connector;
	enum pipe pipe;
	enum igt_commit_style commit;
} data_connector_t;

//TODO: data_tile_group_t, add comments

static int drm_property_is_tile(drmModePropertyPtr prop)
{
	return ((strcmp(prop->name ? prop->name : "", "TILE") ? 0 : 1) &&
			 drm_property_type_is(prop, DRM_MODE_PROP_BLOB));
}

static void get_connector_tile_props(data_t *data, drmModeConnectorPtr conn,
		igt_tile_info_t *tile)
{
	int i = 0;
	drmModePropertyPtr prop;
	drmModePropertyBlobPtr blob;

	for (i = 0; i < conn->count_props; i++) {
		prop = drmModeGetProperty(data->drm_fd, conn->props[i]);

		igt_assert(prop);

		if (!drm_property_is_tile(prop)) continue;

		blob = drmModeGetPropertyBlob(data->drm_fd,
							conn->prop_values[i]);

		if (!blob) return;

		igt_parse_connector_tile_blob(blob, tile);
		break;
	}
}

static void get_number_of_h_tiles(data_t *data)
{
	int i;
	drmModeRes *res;
	drmModeConnector *connector;
	igt_tile_info_t tile = {.num_h_tile = 0};

	igt_assert(res = drmModeGetResources(data->drm_fd));

	for (i = 0; i < res->count_connectors; i++) {
		connector = drmModeGetConnectorCurrent(data->drm_fd,
						res->connectors[i]);

		igt_assert(connector);

		if (!((connector->connection == DRM_MODE_CONNECTED) &&
			(connector->connector_type == DRM_MODE_CONNECTOR_DisplayPort)))
				continue;

		get_connector_tile_props(data, connector, &tile);
		data->num_h_tiles = tile.num_h_tile;
		break;
	}
}

static void set_test_pattern_fb(data_t *data, data_connector_t *conn_data)
{
	int count = 0;
	bool is_tile = false;
	enum pipe pipe;
	igt_tile_info_t tile;
	igt_output_t *output;
	igt_plane_t *primary;
	struct igt_fb fb_test_pattern;

	igt_create_pattern_fb(data->drm_fd,
							7840,
							4320,
							DRM_FORMAT_XBGR8888,
							LOCAL_DRM_FORMAT_MOD_NONE,
							&fb_test_pattern);

	data->fb_test_pattern = &fb_test_pattern;

	for_each_connected_output(data->display, output)
	{
		is_tile = false;

		conn_data[count].connector = drmModeGetConnector(data->display->drm_fd,
														 output->id);

		igt_assert(conn_data[count].connector);

		if (!(conn_data[count].connector->connector_type ==
						DRM_MODE_CONNECTOR_DisplayPort))
			continue;

		get_connector_tile_props(data, conn_data[count].connector, &tile);
		
		conn_data[count].tile = &tile;
		is_tile = true;

		if (!is_tile) continue;

		output = igt_output_from_connector(data->display,
										   conn_data[count].connector);

		for_each_pipe(data->display, pipe) {
			if (count > 0 && pipe == conn_data[count-1].pipe) continue;

			if (igt_pipe_connector_valid(pipe, output)) {
				conn_data[count].pipe = pipe;
				conn_data[count].output = output;

				igt_output_set_pipe(conn_data[count].output,
							conn_data[count].pipe);

				primary = igt_output_get_plane_type(conn_data[count].output,
							DRM_PLANE_TYPE_PRIMARY);

				igt_plane_set_fb(primary, &fb_test_pattern);

				igt_fb_set_size(&fb_test_pattern, primary,
							conn_data[count].tile->tile_h_size,
							conn_data[count].tile->tile_v_size);

				igt_fb_set_position(&fb_test_pattern,
							primary,
							(conn_data[count].tile->tile_h_size *
							conn_data[count].tile->tile_h_loc),
							(conn_data[count].tile->tile_v_size *
							conn_data[count].tile->tile_v_loc));

				igt_plane_set_size(primary,
							conn_data[count].tile->tile_h_size,
							conn_data[count].tile->tile_v_size);
				break;
			}
		}
		count++;
	}
}

static void test_cleanup(data_t *data, data_connector_t *conn)
{
	igt_plane_t *primary;

	for (int i = 0; i < data->num_h_tiles; i++) {
		if (conn[i].output) {
			primary = igt_output_get_plane_type( conn[i].output, DRM_PLANE_TYPE_PRIMARY);
			igt_plane_set_fb(primary, NULL);
			igt_display_commit2(data->display, data->commit);
			igt_remove_fb(data->drm_fd, data->fb_test_pattern);

			igt_output_set_pipe(conn[i].output, PIPE_NONE);
			igt_display_commit2(data->display, data->commit);
		}
	}
}

static void page_flip_handler(int fd, unsigned seq,
                                      unsigned tv_sec,
                                      unsigned tv_usec,
                                      unsigned crtc_id, void *data)
{
	static int i = 1;
	igt_info("Event %d\n",i);
	igt_info("tv_sec:%u tv_usec:%u\n", tv_sec, tv_usec);
}

igt_main
{
	int rc = 10;
	struct pollfd pfd;
	data_t data = {.drm_fd = 0, .num_h_tiles = 0,
				   .display = NULL, .commit = COMMIT_LEGACY};
	data_connector_t *conn_data = NULL;
	igt_display_t display;

	drmEventContext event = {
                .version = 3,
                .page_flip_handler2 = page_flip_handler,
				//.vblank_handler = vblank_handler,
        };

	drmModeAtomicReqPtr req = NULL;

	igt_fixture {
		data.drm_fd = drm_open_driver_master(DRIVER_ANY);
		kmstest_set_vt_graphics_mode();
		igt_display_require(&display, data.drm_fd);
		igt_display_reset(&display);
		igt_require_gem(data.drm_fd);
		data.display = &display;

		get_number_of_h_tiles(&data);
		igt_debug("Number of Horizontal Tiles: %d\n", data.num_h_tiles);

		data.commit = data.display->is_atomic ? COMMIT_ATOMIC : COMMIT_LEGACY;
		pfd.fd = data.drm_fd;
		pfd.events = POLLIN;
		pfd.revents = 0;
	}

	if (data.num_h_tiles > 0)
		conn_data = malloc(data.num_h_tiles * sizeof(data_connector_t));

	igt_subtest("basic-test-pattern") {

		igt_skip_on(data.num_h_tiles == 0);
		igt_assert(conn_data);
		set_test_pattern_fb(&data, conn_data);
		
		igt_display_commit2(data.display, data.commit);

		// igt_display_commit_atomic(data.display, DRM_MODE_ATOMIC_NONBLOCK |
		// DRM_MODE_PAGE_FLIP_EVENT, NULL);

		// drmModeAtomicCommit(data.drm_fd, req, DRM_MODE_ATOMIC_NONBLOCK | 
		// DRM_MODE_PAGE_FLIP_EVENT, NULL);

		igt_debug("Commit done \n");
		rc = poll(&pfd, 1, 1000);
		igt_info("rc val = %d\n",rc);
		if (rc == 1) {
			igt_info("POLL\n");
			drmHandleEvent(data.drm_fd, &event);
		}

		test_cleanup(&data, conn_data);
	}

	igt_fixture {
		free(conn_data);
		close(data.drm_fd);
		kmstest_restore_vt_mode();
		igt_display_fini(data.display);
	}
}
